var tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>";
	var tabCounter = 2;    
var container=$(".container");	
	var tabs = $( "#tabs" ).tabs( );
$(function () {
	url = "data.php"; 
	$("#tags").val('');
	params = {
		type : "request"
	}
	respon = sendAjax(url, params);

	var availableTags;
	respon.error(function (xhr, status, msg) {
		console.log("Error");
		console.log(status);
		console.log(msg);
		console.log(xhr);

	});
//{ event: "mouseover"});   

	
	respon.success(function (result, status) {
		console.log(result.tags);
		listCari = result.cari;
		pokemon = result.pokemon;
		$("#tags").autocomplete({
			source : result.tags,
			focus : function (event, ui) {
				$("#tags").val(ui.item.label);
				return false;
			},
			select : function (event, ui) {
				$("#tags").val(ui.item.value);
				//$("#project-id").val(ui.item.value);
				key = ui.item.key;
				console.log(listCari[key]);
				$("#list-pokemon").empty().html('List<ol>');
				//$("#project-icon").attr("src", "images/" + ui.item.icon);
				for(i=0;i<listCari[key].length;i++){
					key2=listCari[key][i];
					detail=pokemon[key2];
					txt="<li>"+detail[1]+"<button onclick='openDetail("+key2+")'>";
					txt=txt+"detail</button";
					$("#list-pokemon").append(txt);
				}
				$("#list-pokemon").append("</ol>");
				return false;
			}
		});
	});

	dialog = $("#dialog").dialog({
			autoOpen : false,
			height : 300,
			width : 350,
			modal : true,
			buttons : {				
				Close : function () {
					dialog.dialog("close");
				}
			},
			close : function () {
				 
			}
		});

});

function sendAjax(url, params) {
	var request = jQuery.ajax({
			url : url,
			type : "POST",
			data : params,
			dataType : "json",
			cache : false,
			timeout : 20000,
		});

	return request;
}

function openDetail(pok) {

	detail = pokemon[pok];
	console.log(detail);
/*
	$("#pokName").html(detail[1]);
	$("#pokType").html(detail[2]);
	$("#pokEvolusi").html(detail[3]);
	dialog.dialog("open");
*/
      var label = detail[1];// + tabCounter,
        id = "tabs-" + tabCounter,
        li = $( tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) ),
        tabContentHtml = "<div class='pokImage' style='background-image:url(image.php?target="+detail[0]+".jpg)'></div>";
		tabContentHtml = tabContentHtml+"<div class='pokName'>"+detail[1]+"</div>";
		tabContentHtml = tabContentHtml+"<div class='pokType'>Type :"+detail[2]+"</div>";
		tabContentHtml = tabContentHtml+"<div class='pokEvolusi'>Next Evolution: "+detail[3]+"</div>";
		
		tabContentHtml = tabContentHtml+"<div class='clear'></div>"; 
		tabContentHtml = tabContentHtml+"<div class='pokDetail'> "+detail[4]+"</div>";
		
      container.find( ".ui-tabs-nav" ).append( li );
      $("#tabs").append( "<div id='" + id + "'><p>" + tabContentHtml + "</p></div>" );
      tabs.tabs( "refresh" );
	  tabs.tabs( "option", "active", tabCounter );
	  
      tabCounter++;
}

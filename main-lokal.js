var tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>";
	var tabCounter = 2;    
var container=$(".container");	
	var tabs = $( "#tabs" ).tabs( );
var respon = {
	"pokemon" : [["1", " Pikachu", "Electric", "Raichu"], ["2", "Raichu", "Electric", "none"], ["3", "Mew", "Psychic", "none"], ["4", "Mewtwo ", "Psychic", "none"], ["5", "Chikorita", "Grass", "Bayleef"], ["6", "Bayleef", "Grass", "Meganium"], ["7", "Meganium", "Grass", "none"], ["8", "Cyndaquil", "Fire", "Quilava"], ["9", "Quilava", "Fire", "Typholison"], ["10", "Typholison", "Fire", "none"], ["11", "Trotodile", "Water", "Croconaw"], ["12", "Croconaw", "Water", "Feraligatr"], ["13", "Feligatr", "Water", "none"], ["14", "Sentret", "Normal", "Furet"], ["15", "Furet", "Normal", "none"], ["16", "Hoothoot", "Normal, Flying", "Noctowl"], ["17", "Noctowl", "Normal, Flying", "None"], ["18", "Ledyba", "Bug, Flying", "Ledian"], ["19", "Ledian", "Bug, Flying", "none"], ["20", "Spinarak", "Bug, Poison", "Ariados"], ["21", "Ariados", "Bug, Poison", "none"], ["22", "Zubat", "Poison, Flying", "Golbat"], ["23", "Golbat", "Poison, Flying", "Crobat"], ["24", "Crobat", "Poison, Flying", "none"], ["25", "Oddish", "Grass, Poison", "Gloom"], ["25", "Gloom", "Grass, Poison", "Vileplume"], ["26", "Vileplume", "Grass, Poison", "none"], ["27", "Paras", "Bug, Grass", "Parasect"], ["28", "Parasect", "Bug, Grass", "none"], ["29", "Venonat", "Bug, Poison", "Venomoth"], ["30", "Venomoth", "Bug, Poison", "none"], ["31", "Diglett", "Ground", "Dugtrio"], ["32", "Dugtrio", "Ground", "none"], ["33", "Meowth", "Normal", "Persian"], ["34", "Persian", "Normal", "None"], ["35", "Psyduck", "Water", "Goldduck"], ["36", "Goldduck", "Water", "None"], ["37", "Mankey", "Fighting", "Primeape"], ["38", "Primeape", "Fighting", "none"], ["39", "Growlithe", "Fire", "Arcanine"], ["40", "Arcanine", "Fire", "none"], ["41", "Poliwag", "Water", "Poliwhirl"], ["42", "Poliwhirl", "Water", "Poliwrath \/ Politoed"], ["43", "Poliwrath", "Water, Fighting", "none"], ["44", "Politoed", "Water ", "none"], ["45", "Abra", "Psychic", "Kadabra"], ["46", "Kadabra", "Psychic", "Alakazam"], ["47", "Alakazam", "Psychic", "none"], ["48", "Hoppip", "Grass, Flying", "Skiploom"], ["49", "Skiploom", "Grass, Flying", "Jumpluff"], ["50", "Jumpluff", "Grass, Flying", "none"]],
	"cari" : {
		"pikachu" : [0],
		"electric" : [0, 1],
		"evolusi ke raichu" : [0],
		"raichu" : [1],
		"mew" : [2],
		"psychic" : [2, 3, 45, 46, 47],
		"mewtwo" : [3],
		"chikorita" : [4],
		"grass" : [4, 5, 6, 24, 25, 26, 27, 28, 48, 49, 50],
		"evolusi ke bayleef" : [4],
		"bayleef" : [5],
		"evolusi ke meganium" : [5],
		"meganium" : [6],
		"cyndaquil" : [7],
		"fire" : [7, 8, 9, 39, 40],
		"evolusi ke quilava" : [7],
		"quilava" : [8],
		"evolusi ke typholison" : [8],
		"typholison" : [9],
		"trotodile" : [10],
		"water" : [10, 11, 12, 35, 36, 41, 42, 43, 44],
		"evolusi ke croconaw" : [10],
		"croconaw" : [11],
		"evolusi ke feraligatr" : [11],
		"feligatr" : [12],
		"sentret" : [13],
		"normal" : [13, 14, 15, 16, 33, 34],
		"evolusi ke furet" : [13],
		"furet" : [14],
		"hoothoot" : [15],
		"flying" : [15, 16, 17, 18, 21, 22, 23, 48, 49, 50],
		"evolusi ke noctowl" : [15],
		"noctowl" : [16],
		"ledyba" : [17],
		"bug" : [17, 18, 19, 20, 27, 28, 29, 30],
		"evolusi ke ledian" : [17],
		"ledian" : [18],
		"spinarak" : [19],
		"poison" : [19, 20, 21, 22, 23, 24, 25, 26, 29, 30],
		"evolusi ke ariados" : [19],
		"ariados" : [20],
		"zubat" : [21],
		"evolusi ke golbat" : [21],
		"golbat" : [22],
		"evolusi ke crobat" : [22],
		"crobat" : [23],
		"oddish" : [24],
		"evolusi ke gloom" : [24],
		"gloom" : [25],
		"evolusi ke vileplume" : [25],
		"vileplume" : [26],
		"paras" : [27],
		"evolusi ke parasect" : [27],
		"parasect" : [28],
		"venonat" : [29],
		"evolusi ke venomoth" : [29],
		"venomoth" : [30],
		"diglett" : [31],
		"ground" : [31, 32],
		"evolusi ke dugtrio" : [31],
		"dugtrio" : [32],
		"meowth" : [33],
		"evolusi ke persian" : [33],
		"persian" : [34],
		"psyduck" : [35],
		"evolusi ke goldduck" : [35],
		"goldduck" : [36],
		"mankey" : [37],
		"fighting" : [37, 38, 43],
		"evolusi ke primeape" : [37],
		"primeape" : [38],
		"growlithe" : [39],
		"evolusi ke arcanine" : [39],
		"arcanine" : [40],
		"poliwag" : [41],
		"evolusi ke poliwhirl" : [41],
		"poliwhirl" : [42],
		"evolusi ke poliwrath \/ politoed" : [42],
		"poliwrath" : [43],
		"politoed" : [44],
		"abra" : [45],
		"evolusi ke kadabra" : [45],
		"kadabra" : [46],
		"evolusi ke alakazam" : [46],
		"alakazam" : [47],
		"hoppip" : [48],
		"evolusi ke skiploom" : [48],
		"skiploom" : [49],
		"evolusi ke jumpluff" : [49],
		"jumpluff" : [50]
	},
	"tags" : [{
			"value" : "Pikachu",
			"label" : "Pikachu",
			"key" : "pikachu"
		}, {
			"value" : "Electric",
			"label" : "Electric",
			"key" : "electric"
		}, {
			"value" : "Evolusi ke Raichu",
			"label" : "Evolusi ke Raichu",
			"key" : "evolusi ke raichu"
		}, {
			"value" : "Raichu",
			"label" : "Raichu",
			"key" : "raichu"
		}, {
			"value" : "Mew",
			"label" : "Mew",
			"key" : "mew"
		}, {
			"value" : "Psychic",
			"label" : "Psychic",
			"key" : "psychic"
		}, {
			"value" : "Mewtwo",
			"label" : "Mewtwo",
			"key" : "mewtwo"
		}, {
			"value" : "Chikorita",
			"label" : "Chikorita",
			"key" : "chikorita"
		}, {
			"value" : "Grass",
			"label" : "Grass",
			"key" : "grass"
		}, {
			"value" : "Evolusi ke Bayleef",
			"label" : "Evolusi ke Bayleef",
			"key" : "evolusi ke bayleef"
		}, {
			"value" : "Bayleef",
			"label" : "Bayleef",
			"key" : "bayleef"
		}, {
			"value" : "Evolusi ke Meganium",
			"label" : "Evolusi ke Meganium",
			"key" : "evolusi ke meganium"
		}, {
			"value" : "Meganium",
			"label" : "Meganium",
			"key" : "meganium"
		}, {
			"value" : "Cyndaquil",
			"label" : "Cyndaquil",
			"key" : "cyndaquil"
		}, {
			"value" : "Fire",
			"label" : "Fire",
			"key" : "fire"
		}, {
			"value" : "Evolusi ke Quilava",
			"label" : "Evolusi ke Quilava",
			"key" : "evolusi ke quilava"
		}, {
			"value" : "Quilava",
			"label" : "Quilava",
			"key" : "quilava"
		}, {
			"value" : "Evolusi ke Typholison",
			"label" : "Evolusi ke Typholison",
			"key" : "evolusi ke typholison"
		}, {
			"value" : "Typholison",
			"label" : "Typholison",
			"key" : "typholison"
		}, {
			"value" : "Trotodile",
			"label" : "Trotodile",
			"key" : "trotodile"
		}, {
			"value" : "Water",
			"label" : "Water",
			"key" : "water"
		}, {
			"value" : "Evolusi ke Croconaw",
			"label" : "Evolusi ke Croconaw",
			"key" : "evolusi ke croconaw"
		}, {
			"value" : "Croconaw",
			"label" : "Croconaw",
			"key" : "croconaw"
		}, {
			"value" : "Evolusi ke Feraligatr",
			"label" : "Evolusi ke Feraligatr",
			"key" : "evolusi ke feraligatr"
		}, {
			"value" : "Feligatr",
			"label" : "Feligatr",
			"key" : "feligatr"
		}, {
			"value" : "Sentret",
			"label" : "Sentret",
			"key" : "sentret"
		}, {
			"value" : "Normal",
			"label" : "Normal",
			"key" : "normal"
		}, {
			"value" : "Evolusi ke Furet",
			"label" : "Evolusi ke Furet",
			"key" : "evolusi ke furet"
		}, {
			"value" : "Furet",
			"label" : "Furet",
			"key" : "furet"
		}, {
			"value" : "Hoothoot",
			"label" : "Hoothoot",
			"key" : "hoothoot"
		}, {
			"value" : "flying",
			"label" : "flying",
			"key" : "flying"
		}, {
			"value" : "Evolusi ke Noctowl",
			"label" : "Evolusi ke Noctowl",
			"key" : "evolusi ke noctowl"
		}, {
			"value" : "Noctowl",
			"label" : "Noctowl",
			"key" : "noctowl"
		}, {
			"value" : "Ledyba",
			"label" : "Ledyba",
			"key" : "ledyba"
		}, {
			"value" : "Bug",
			"label" : "Bug",
			"key" : "bug"
		}, {
			"value" : "Evolusi ke Ledian",
			"label" : "Evolusi ke Ledian",
			"key" : "evolusi ke ledian"
		}, {
			"value" : "Ledian",
			"label" : "Ledian",
			"key" : "ledian"
		}, {
			"value" : "Spinarak",
			"label" : "Spinarak",
			"key" : "spinarak"
		}, {
			"value" : "poison",
			"label" : "poison",
			"key" : "poison"
		}, {
			"value" : "Evolusi ke Ariados",
			"label" : "Evolusi ke Ariados",
			"key" : "evolusi ke ariados"
		}, {
			"value" : "Ariados",
			"label" : "Ariados",
			"key" : "ariados"
		}, {
			"value" : "Zubat",
			"label" : "Zubat",
			"key" : "zubat"
		}, {
			"value" : "Poison",
			"label" : "Poison",
			"key" : "poison"
		}, {
			"value" : "Evolusi ke Golbat",
			"label" : "Evolusi ke Golbat",
			"key" : "evolusi ke golbat"
		}, {
			"value" : "Golbat",
			"label" : "Golbat",
			"key" : "golbat"
		}, {
			"value" : "Evolusi ke Crobat",
			"label" : "Evolusi ke Crobat",
			"key" : "evolusi ke crobat"
		}, {
			"value" : "Crobat",
			"label" : "Crobat",
			"key" : "crobat"
		}, {
			"value" : "Oddish",
			"label" : "Oddish",
			"key" : "oddish"
		}, {
			"value" : "Evolusi ke Gloom",
			"label" : "Evolusi ke Gloom",
			"key" : "evolusi ke gloom"
		}, {
			"value" : "Gloom",
			"label" : "Gloom",
			"key" : "gloom"
		}, {
			"value" : "Evolusi ke Vileplume",
			"label" : "Evolusi ke Vileplume",
			"key" : "evolusi ke vileplume"
		}, {
			"value" : "Vileplume",
			"label" : "Vileplume",
			"key" : "vileplume"
		}, {
			"value" : "Paras",
			"label" : "Paras",
			"key" : "paras"
		}, {
			"value" : "grass",
			"label" : "grass",
			"key" : "grass"
		}, {
			"value" : "Evolusi ke Parasect",
			"label" : "Evolusi ke Parasect",
			"key" : "evolusi ke parasect"
		}, {
			"value" : "Parasect",
			"label" : "Parasect",
			"key" : "parasect"
		}, {
			"value" : "Venonat",
			"label" : "Venonat",
			"key" : "venonat"
		}, {
			"value" : "Evolusi ke Venomoth",
			"label" : "Evolusi ke Venomoth",
			"key" : "evolusi ke venomoth"
		}, {
			"value" : "Venomoth",
			"label" : "Venomoth",
			"key" : "venomoth"
		}, {
			"value" : "Diglett",
			"label" : "Diglett",
			"key" : "diglett"
		}, {
			"value" : "Ground",
			"label" : "Ground",
			"key" : "ground"
		}, {
			"value" : "Evolusi ke Dugtrio",
			"label" : "Evolusi ke Dugtrio",
			"key" : "evolusi ke dugtrio"
		}, {
			"value" : "Dugtrio",
			"label" : "Dugtrio",
			"key" : "dugtrio"
		}, {
			"value" : "Meowth",
			"label" : "Meowth",
			"key" : "meowth"
		}, {
			"value" : "Evolusi ke Persian",
			"label" : "Evolusi ke Persian",
			"key" : "evolusi ke persian"
		}, {
			"value" : "Persian",
			"label" : "Persian",
			"key" : "persian"
		}, {
			"value" : "Psyduck",
			"label" : "Psyduck",
			"key" : "psyduck"
		}, {
			"value" : "Evolusi ke Goldduck",
			"label" : "Evolusi ke Goldduck",
			"key" : "evolusi ke goldduck"
		}, {
			"value" : "Goldduck",
			"label" : "Goldduck",
			"key" : "goldduck"
		}, {
			"value" : "Mankey",
			"label" : "Mankey",
			"key" : "mankey"
		}, {
			"value" : "Fighting",
			"label" : "Fighting",
			"key" : "fighting"
		}, {
			"value" : "Evolusi ke Primeape",
			"label" : "Evolusi ke Primeape",
			"key" : "evolusi ke primeape"
		}, {
			"value" : "Primeape",
			"label" : "Primeape",
			"key" : "primeape"
		}, {
			"value" : "Growlithe",
			"label" : "Growlithe",
			"key" : "growlithe"
		}, {
			"value" : "Evolusi ke Arcanine",
			"label" : "Evolusi ke Arcanine",
			"key" : "evolusi ke arcanine"
		}, {
			"value" : "Arcanine",
			"label" : "Arcanine",
			"key" : "arcanine"
		}, {
			"value" : "Poliwag",
			"label" : "Poliwag",
			"key" : "poliwag"
		}, {
			"value" : "Evolusi ke Poliwhirl",
			"label" : "Evolusi ke Poliwhirl",
			"key" : "evolusi ke poliwhirl"
		}, {
			"value" : "Poliwhirl",
			"label" : "Poliwhirl",
			"key" : "poliwhirl"
		}, {
			"value" : "Evolusi ke Poliwrath \/ politoed",
			"label" : "Evolusi ke Poliwrath \/ politoed",
			"key" : "evolusi ke poliwrath \/ politoed"
		}, {
			"value" : "Poliwrath",
			"label" : "Poliwrath",
			"key" : "poliwrath"
		}, {
			"value" : "fighting",
			"label" : "fighting",
			"key" : "fighting"
		}, {
			"value" : "Politoed",
			"label" : "Politoed",
			"key" : "politoed"
		}, {
			"value" : "Abra",
			"label" : "Abra",
			"key" : "abra"
		}, {
			"value" : "Evolusi ke Kadabra",
			"label" : "Evolusi ke Kadabra",
			"key" : "evolusi ke kadabra"
		}, {
			"value" : "Kadabra",
			"label" : "Kadabra",
			"key" : "kadabra"
		}, {
			"value" : "Evolusi ke Alakazam",
			"label" : "Evolusi ke Alakazam",
			"key" : "evolusi ke alakazam"
		}, {
			"value" : "Alakazam",
			"label" : "Alakazam",
			"key" : "alakazam"
		}, {
			"value" : "Hoppip",
			"label" : "Hoppip",
			"key" : "hoppip"
		}, {
			"value" : "Evolusi ke Skiploom",
			"label" : "Evolusi ke Skiploom",
			"key" : "evolusi ke skiploom"
		}, {
			"value" : "Skiploom",
			"label" : "Skiploom",
			"key" : "skiploom"
		}, {
			"value" : "Evolusi ke Jumpluff",
			"label" : "Evolusi ke Jumpluff",
			"key" : "evolusi ke jumpluff"
		}, {
			"value" : "Jumpluff",
			"label" : "Jumpluff",
			"key" : "jumpluff"
		}
	]
}

$(function () {
	url = "data.json"; 
	$("#tags").val('');
	params = {
		type : "request"
	}
	//respon = sendAjax(url, params);

	var availableTags;
/*	
	respon.error(function (xhr, status, msg) {
		console.log("Error");
		console.log(status);
		console.log(msg);
		console.log(xhr);

	});
//{ event: "mouseover"});   

	
	respon.success(function (result, status) {
*/
		result=respon;
		listCari = result.cari;
		pokemon = result.pokemon;
		$("#tags").autocomplete({
			source : result.tags,
			focus : function (event, ui) {
				$("#tags").val(ui.item.label);
				return false;
			},
			select : function (event, ui) {
				$("#tags").val(ui.item.value);
				//$("#project-id").val(ui.item.value);
				key = ui.item.key;
				console.log(listCari[key]);
				$("#list-pokemon").empty().html('List<ol>');
				//$("#project-icon").attr("src", "images/" + ui.item.icon);
				for(i=0;i<listCari[key].length;i++){
					key2=listCari[key][i];
					detail=pokemon[key2];
					txt="<li>"+detail[1]+"<button onclick='openDetail("+key2+")'>";
					txt=txt+"detail</button";
					$("#list-pokemon").append(txt);
				}
				$("#list-pokemon").append("</ol>");
				return false;
			}
		});
//	});

	dialog = $("#dialog").dialog({
			autoOpen : false,
			height : 300,
			width : 350,
			modal : true,
			buttons : {				
				Close : function () {
					dialog.dialog("close");
				}
			},
			close : function () {
				 
			}
		});

});

function sendAjax(url, params) {
	var request = jQuery.ajax({
			url : url,
			type : "POST",
			data : params,
			dataType : "json",
			cache : false,
			timeout : 20000,
		});

	return request;
}

function openDetail(pok) {

	detail = pokemon[pok];
	console.log(detail);
/*
	$("#pokName").html(detail[1]);
	$("#pokType").html(detail[2]);
	$("#pokEvolusi").html(detail[3]);
	dialog.dialog("open");
*/
      var label = detail[1];// + tabCounter,
        id = "tabs-" + tabCounter,
        li = $( tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) ),
        tabContentHtml = "<div class='pokImage' style='background-image:url(assets/image/"+detail[0]+".jpg)'></div>";
		tabContentHtml = tabContentHtml+"<div class='pokName'>"+detail[1]+"</div>";
		tabContentHtml = tabContentHtml+"<div class='pokType'>Type :"+detail[2]+"</div>";
		tabContentHtml = tabContentHtml+"<div class='pokEvolusi'>Next Evolution: "+detail[3]+"</div>";
		tabContentHtml = tabContentHtml+"<div class='clear'></div>"; 
		
      container.find( ".ui-tabs-nav" ).append( li );
      $("#tabs").append( "<div id='" + id + "'><p>" + tabContentHtml + "</p></div>" );
      tabs.tabs( "refresh" );
	  tabs.tabs( "option", "active", tabCounter );
	  
      tabCounter++;
}
